package testes.com.estudo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import testes.com.estudo.model.StatusTitulo;
import testes.com.estudo.model.Titulo;
import testes.com.estudo.repository.Titulos;


@Service
public class CadastroTituloService {
	
	
	@Autowired
	private Titulos titulos;
	
	public void salvar(Titulo titulo) {
		try {
			titulos.save(titulo);
		} catch(DataIntegrityViolationException e) {
			throw new IllegalArgumentException ("Formato de data inválido");
		}
	}

	public String receber(Long codigo) {
		Titulo titulo = titulos.getOne(codigo);
		titulo.setStatus(StatusTitulo.RECEBIDO);
		titulos.save(titulo);
		
		return StatusTitulo.RECEBIDO.getDescricao();
	}

	public void excluir(Long codigo) {
		titulos.delete(codigo);
		
	}
	

}
