package testes.com.estudo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import testes.com.estudo.model.Titulo;

public interface Titulos extends JpaRepository<Titulo ,Long> {

	public List<Titulo> findByDescricaoContaining(String Descricao);
}
